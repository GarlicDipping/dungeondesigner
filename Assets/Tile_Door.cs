﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile_Door : MonoBehaviour
{
	public Animator animator;
	
	public void Open()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Close"))
		{
			animator.SetTrigger("Open");
		}
	}

	public void Close()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
		{
			animator.SetTrigger("Close");
		}
	}
}
