﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
	static Main _ins = null;
	public static Main ins
	{
		get
		{
			if (_ins == null)
			{
				_ins = (Main)FindObjectOfType(typeof(Main));

				if (_ins == null)
				{
					Debug.LogError("No Main OBJ");
					return null;
				}
			}

			return _ins;
		}
	}

	public PrefabSwapper sceneSwapper;

	private void Start()
	{
		sceneSwapper.SwapTo("dungeon");
	}

}
