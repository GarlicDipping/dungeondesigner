﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterRating
{
	C,
	B,
	A,
	S,
	SS,
	SSS
}

public enum TileType
{
	Empty,
	Wall,
	Door
}

public enum SectorType
{
	Empty,
	EnteranceRoom,
	MonsterRoom,
	BossRoom,
	Hallway_LR,
	Hallway_LT,
	Hallway_LB,
	Hallway_TB,
	Hallway_RT,
	Hallway_RB,
	Hallway_BRT,
	Hallway_LBR,
	Hallway_RTL,
	Hallway_TLB,
	Hallway_LTRB,
	Hallway_L,
	Hallway_R,
	Hallway_T,
	Hallway_B
}

[Flags]
public enum Directions : int
{
	None = 0,
	Left = 1,
	Right = 2,
	Top = 4,
	Bottom = 8
}

public static class DirectionsExtensions
{
	public static Directions OppositeDirection(this Directions directions)
	{
		Directions result = Directions.None;
		if (directions.Contains(Directions.Bottom))
		{
			result |= SingleOppositeDirection(Directions.Bottom);
		}
		if (directions.Contains(Directions.Top))
		{
			result |= SingleOppositeDirection(Directions.Top);
		}
		if (directions.Contains(Directions.Left))
		{
			result |= SingleOppositeDirection(Directions.Left);
		}
		if (directions.Contains(Directions.Right))
		{
			result |= SingleOppositeDirection(Directions.Right);
		}
		return result;
	}

	private static Directions SingleOppositeDirection(Directions directions)
	{
		switch (directions)
		{
			case Directions.Bottom:
				return Directions.Top;
			case Directions.Right:
				return Directions.Left;
			case Directions.Left:
				return Directions.Right;
			case Directions.Top:
				return Directions.Bottom;
		}
		return Directions.None;
	}

	public static Directions Flip(this Directions directions)
	{
		Directions allDir = Directions.Left | Directions.Right | Directions.Top | Directions.Bottom;
		return directions ^ allDir;
	}

	public static bool Contains(this Directions directions, Directions dirToCheck)
	{
		return (directions & dirToCheck) != Directions.None;
	}

	public static int CountDirections(this Directions directions)
	{
		int count = 0;
		while(directions != 0)
		{
			count++;
			directions &= directions - 1;
		}
		return count;
	}
}