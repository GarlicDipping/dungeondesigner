﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GarlicUtils
{
	public static Rect RectFromBoxCollider2D(BoxCollider2D collider)
	{
		Vector2 collider_pos = collider.transform.position;
		Vector2 collider_lb = collider_pos - collider.size / 2f + collider.offset;
		Rect r = new Rect(collider_lb, collider.size);
		//Rect r = new Rect(collider.bounds.min, collider.bounds.size);
		return r;
	}
}
