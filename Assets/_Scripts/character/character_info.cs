﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class character_info
{
	public data_character data_Character;
	public int lv = 1;
	public int HP = 1;
	public int MP = 1;
	public int STR = 1;
	public int INT = 1;
	public int DEF = 1;
	public int RES = 1;
}
