﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class data_sector : ScriptableObject
{
	public Sprite sectorIcon;
	public SectorType SectorType;
	public GameObject SectorPrefab;
	public bool DoNotLoad;
}
