﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class data_loader
{
	public static data_sector SectorFromType(SectorType type)
	{
		foreach(var sectorData in Sector_Total_Datas)
		{
			if(sectorData.SectorType == type)
			{
				return sectorData;
			}
		}
		return null;
	}

	public static data_sector SectorHallwayFromDirections(Directions directions)
	{
		if(directions == Directions.None)
		{
			return SectorFromType(SectorType.Empty);
		}

		foreach (var sectorData in Sector_Total_Datas)
		{
			string sectorTypeString = sectorData.SectorType.ToString();
			if (sectorTypeString.StartsWith("Hallway_"))
			{
				sectorTypeString = sectorTypeString.Replace("Hallway_", string.Empty);
				if (SectorTypeIsMatchingDirection(sectorTypeString, directions))
				{
					return sectorData;
				}
			}
		}
		return null;
	}

	static bool SectorTypeIsMatchingDirection(string sectorTypeString, Directions directions)
	{
		Directions sectorTypeStringToDirections = Directions.None;
		if (sectorTypeString.Contains("L"))
		{
			sectorTypeStringToDirections |= Directions.Left;
		}
		if (sectorTypeString.Contains("R"))
		{
			sectorTypeStringToDirections |= Directions.Right;
		}
		if (sectorTypeString.Contains("T"))
		{
			sectorTypeStringToDirections |= Directions.Top;
		}
		if (sectorTypeString.Contains("B"))
		{
			sectorTypeStringToDirections |= Directions.Bottom;
		}

		if(directions == sectorTypeStringToDirections)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static List<data_dungeon> Dungeon_Datas
	{
		get
		{
			return Load_Dungeon_Datas();
		}
	}

	public static List<data_sector> Sector_Loadable_Datas
	{
		get
		{
			return Load_Sector_Datas();
		}
	}

	public static List<data_sector> Sector_Total_Datas
	{
		get
		{
			return Load_Sector_Datas(false);
		}
	}

	public static data_sector BossRoom_Data
	{
		get
		{
			return Resources.Load<data_sector>("SO/sector/sector_bossRoom");
		}
	}

	public static data_sector Enterence_Data
	{
		get
		{
			return Resources.Load<data_sector>("SO/sector/sector_enterenceRoom");
		}
	}

	public static data_sector Empty_Sector
	{
		get
		{
			return Resources.Load<data_sector>("SO/sector/sector_empty");
		}
	}
	static List<data_sector> Load_Sector_Datas(bool blockDoNotLoadDatas = true)
	{
		var sector_datas = Resources.LoadAll<data_sector>("SO/sector");
		List<data_sector> sector_data_list = new List<data_sector>();
		foreach(var sectorData in sector_datas)
		{
			if (blockDoNotLoadDatas)
			{
				if(sectorData.DoNotLoad == false)
				{
					sector_data_list.Add(sectorData);
				}				
			}
			else
			{
				sector_data_list.Add(sectorData);
			}
		}
		return sector_data_list;
	}
	static List<data_dungeon> Load_Dungeon_Datas()
	{
		var sector_datas = Resources.LoadAll<data_dungeon>("SO/dungeon");
		List<data_dungeon> dungeon_data_list = new List<data_dungeon>();
		foreach (var sectorData in sector_datas)
		{		
			dungeon_data_list.Add(sectorData);
		}
		return dungeon_data_list;
	}
}
