﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class character_globals
{
	public static int HPBase = 200;
	public static int MPBase = 50;
	public static int StrBase = 60;
	public static int IntBase = 60;
	public static int DefBase = 20;
	public static int ResBase = 20;

	public static float hp_stat_coeff = 1.05f;
	public static float mp_stat_coeff = 1.05f;
	public static float str_stat_coeff = 1.1f;
	public static float int_stat_coeff = 1.1f;
	public static float def_stat_coeff = 1.1f;
	public static float res_stat_coeff = 1.1f;

	public static float hp_lv_coeff = 1.04f;
	public static float mp_lv_coeff = 1.02f;
	public static float str_lv_coeff = 1.025f;
	public static float int_lv_coeff = 1.025f;
	public static float def_lv_coeff = 1.01f;
	public static float res_lv_coeff = 1.01f;
}

public class data_character : ScriptableObject
{
	public CharacterRating characterRating;
	public int totalStatPoint;
}