﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class SOCreator
{
	[MenuItem("Assets/Create/Data/Sector")]
	public static void Create_SO_sector()
	{
		SO_Util.CreateAsset<data_sector>();
	}

	[MenuItem("Assets/Create/Data/Dungeon")]
	public static void Create_SO_dungeon()
	{
		SO_Util.CreateAsset<data_dungeon>();
	}

	[MenuItem("Assets/Create/Data/Character")]
	public static void Create_SO_character()
	{
		SO_Util.CreateAsset<data_character>();
	}
}
