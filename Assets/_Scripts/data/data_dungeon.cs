﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 던전을 처음 자동생성할때 이용할 정보
/// </summary>
public class data_dungeon : ScriptableObject
{
	public int width, height;
	public IntPoint EnterenceSector;
	public Directions EnterenceDirection;

	public IntPoint BossSector;
}