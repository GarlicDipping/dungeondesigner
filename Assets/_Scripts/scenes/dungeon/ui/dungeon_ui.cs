﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeon_ui : MonoBehaviour
{
	public dungeon_ui_sectorTypeSelector sectorTypeSelector;
	public dungeon_ui_dragOptions dragOptionSelector;
	public dungeon_ui_playModeSelector playModeSelector;
	public void Initialize()
	{
		sectorTypeSelector.Initialize();
		playModeSelector.Initialize();
	}
	
}
