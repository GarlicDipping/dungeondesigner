﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeon_ui_playModeSelector : MonoBehaviour
{
	public GameObject Play, Edit;

	public void Initialize()
	{
		Refresh();
	}

	public void OnPlayClicked()
	{
		if (dungeon_manager.ins.CanBeginPlay())
		{
			dungeon_manager.ins.BeginPlay();
			Refresh();
		}
	}

	public void OnEditClicked()
	{
		dungeon_manager.ins.StopPlay();
		Refresh();
	}

	void Refresh()
	{
		switch (dungeon_manager.ins.state)
		{
			case dungeon_manager.State.Edit:
				Play.SetActive(true);
				Edit.SetActive(false);
				break;
			case dungeon_manager.State.Play:
				Play.SetActive(false);
				Edit.SetActive(true);
				break;
		}
	}
}
