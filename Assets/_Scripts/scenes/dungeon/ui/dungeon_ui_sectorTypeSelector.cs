﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dungeon_ui_sectorTypeSelector : MonoBehaviour
{
	public GameObject SectorIconParent;
	public GameObject SectorIconButtonPrefab;
	public ScrollRect scrollRect;
	List<data_sector> sector_datas;
	RectTransform rt;
	List<Toggle> sector_toggles;
	// Use this for initialization
	void Awake ()
	{
		rt = GetComponent<RectTransform>();
		sector_toggles = new List<Toggle>();
	}

	public void Initialize()
	{
		sector_datas = data_loader.Sector_Loadable_Datas;
		SectorIconParent.GetComponent<RectTransform>().sizeDelta = new Vector2(
				48 * sector_datas.Count,
				48
			);
		for (int i = 0; i < sector_datas.Count; i++)
		{
			GameObject iconObj = Instantiate(SectorIconButtonPrefab);
			dungeon_ui_sectorTypeIcon uiscr = iconObj.GetComponent<dungeon_ui_sectorTypeIcon>();
			uiscr.icon.sprite = sector_datas[i].sectorIcon;
			uiscr.sector_data = sector_datas[i];
			iconObj.transform.SetParent(SectorIconParent.transform, false);
			uiscr.Init(this);
			sector_toggles.Add(iconObj.GetComponent<Toggle>());
		}
	}

	List<data_sector> SectorDatasToActivate()
	{
		List<data_sector> avaliable_sectors = new List<data_sector>();
		avaliable_sectors.AddRange(sector_datas);
		IntPoint selected_sector = dungeon_manager.ins.inputHandler.CurrentSectorPointClicked;
		List<data_sector> datas_to_remove = new List<data_sector>();

		if (selected_sector.x == 0)
		{
			//LeftMost
			foreach(var sectorData in avaliable_sectors)
			{
				string name_reformed = sectorData.name.Replace("sector_", string.Empty);
				if (name_reformed.Contains("hallway"))
				{
					name_reformed = name_reformed.Replace("hallway", string.Empty);
					if (name_reformed.Contains("L"))
					{
						datas_to_remove.Add(sectorData);
					}
				}
			}
		}
		else if (selected_sector.x == dungeon_manager.ins.dungeon_loader.dungeonWidth - 1)
		{
			//LeftMost
			foreach (var sectorData in avaliable_sectors)
			{
				string name_reformed = sectorData.name.Replace("sector_", string.Empty);
				if (name_reformed.Contains("hallway"))
				{
					name_reformed = name_reformed.Replace("hallway", string.Empty);
					if (name_reformed.Contains("R"))
					{
						datas_to_remove.Add(sectorData);
					}
				}
			}
		}

		if (selected_sector.y == 0)
		{
			//LeftMost
			foreach (var sectorData in avaliable_sectors)
			{
				string name_reformed = sectorData.name.Replace("sector_", string.Empty);
				if (name_reformed.Contains("hallway"))
				{
					name_reformed = name_reformed.Replace("hallway", string.Empty);
					if (name_reformed.Contains("B"))
					{
						datas_to_remove.Add(sectorData);
					}
				}
			}
		}
		else if (selected_sector.y == dungeon_manager.ins.dungeon_loader.dungeonHeight - 1)
		{
			//LeftMost
			foreach (var sectorData in avaliable_sectors)
			{
				string name_reformed = sectorData.name.Replace("sector_", string.Empty);
				if (name_reformed.Contains("hallway"))
				{
					name_reformed = name_reformed.Replace("hallway", string.Empty);
					if (name_reformed.Contains("T"))
					{
						datas_to_remove.Add(sectorData);
					}
				}
			}
		}

		foreach (var data_to_remove in datas_to_remove)
		{
			avaliable_sectors.Remove(data_to_remove);
			//foreach (var sector_toggle in sector_toggles)
			//{
			//	var sector_toggle_scr = sector_toggle.GetComponent<dungeon_ui_sectorTypeIcon>();
			//	if (sector_toggle_scr.sector_data == data_to_remove)
			//	{
			//		sector_toggle_scr.gameObject.SetActive(false);
			//	}
			//}
		}
		return avaliable_sectors;
	}

	char[] GetSectorInvalidDirection(IntPoint sectorPoint)
	{
		List<char> invalid_directions = new List<char>();
		if(sectorPoint.x == 0)
		{
			invalid_directions.Add('L');
		}
		else if(sectorPoint.x == dungeon_manager.ins.dungeon_loader.dungeonWidth - 1)
		{
			invalid_directions.Add('R');
		}

		if (sectorPoint.y == 0)
		{
			invalid_directions.Add('B');
		}
		else if (sectorPoint.y == dungeon_manager.ins.dungeon_loader.dungeonHeight - 1)
		{
			invalid_directions.Add('T');
		}
		return invalid_directions.ToArray();
	}

	public void OnSectorRefreshed()
	{
		gameObject.SetActive(false);

		foreach(var sector_toggle in sector_toggles)
		{
			sector_toggle.isOn = false;
			sector_toggle.gameObject.SetActive(false);
		}
		var sector_datas_to_activate = SectorDatasToActivate();

		foreach (var sector_data in sector_datas_to_activate)
		{
			foreach (var sector_toggle in sector_toggles)
			{
				var sector_toggle_scr = sector_toggle.GetComponent<dungeon_ui_sectorTypeIcon>();
				if (sector_toggle_scr.sector_data == sector_data)
				{
					sector_toggle_scr.gameObject.SetActive(true);
					break;
				}
			}
		}

		//이렇게 gameObject de-reactive 안하면 스크롤뷰 레이아웃이 꼬임
		gameObject.SetActive(true);
	}

	void ForceRebuild()
	{
		LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
	}

	public void ResetSectorSelectorToggles()
	{
		foreach (var sector_toggle in sector_toggles)
		{
			sector_toggle.isOn = false;
		}
	}
}
