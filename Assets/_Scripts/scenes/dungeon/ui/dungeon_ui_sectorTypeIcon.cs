﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dungeon_ui_sectorTypeIcon : MonoBehaviour
{
	public Image icon;
	public data_sector sector_data;
	dungeon_ui_sectorTypeSelector selector;
	public void Init(dungeon_ui_sectorTypeSelector selector)
	{
		this.selector = selector;
	}

	public void OnSectorIconClick(bool selected)
	{
		if (selected)
		{			
			Toggle toggle = GetComponent<Toggle>();
			if (toggle.isOn)
			{
				dungeon_manager.ins.SetRuntimeSelectedSector(sector_data);
				selector.ResetSectorSelectorToggles();
				return;
			}
		}
	}
}
