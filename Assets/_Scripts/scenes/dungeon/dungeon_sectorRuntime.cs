﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class dungeon_sectorRuntime : MonoBehaviour
{
	static Color BG_COLOR_SELECTED = new Color(223f / 255f, 223f / 255f, 1f);
	public TextMeshPro directionText;

	[HideInInspector]
	public data_sector sectorData;
	[HideInInspector]
	public GameObject sectorTilesGO;
	Renderer sectorBGRenderer;
	public IntPoint sectorPos
	{
		get
		{
			return new IntPoint(sectorX, sectorY);
		}
	}
	public int sectorX { get; private set; }
	public int sectorY { get; private set; }
	Color initialSectorColor;

	class HallwayDirectionHandler
	{
		Action onDirectionChanged;
		public HallwayDirectionHandler(Action onDirectionChanged)
		{
			this.onDirectionChanged = onDirectionChanged;
		}
		/// <summary>
		/// 복도의 경우 복도가 열린 방향을, 몬스터룸 등의 방일 경우 방이 열린 방향을 표기
		/// </summary>
		public Directions HallwayDirection { get; private set; }
		
		public void ToggleHallway(Directions dir)
		{
			HallwayDirection ^= dir;
			if (onDirectionChanged != null)
			{
				onDirectionChanged.Invoke();
			}
		}

		public void ConstructHallway(Directions dir)
		{
			if (HallwayDirection.Contains(dir) == false)
			{
				HallwayDirection |= dir;
				if (onDirectionChanged != null)
				{
					onDirectionChanged.Invoke();
				}
			}
		}

		public void DestroyHallway(Directions dir)
		{
			if (HallwayDirection.Contains(dir) == true)
			{
				dir = (HallwayDirection & dir);
				HallwayDirection ^= dir;
				if (onDirectionChanged != null)
				{
					onDirectionChanged.Invoke();
				}
			}
		}

		public void DestroyEveryHallways()
		{
			var directionBefore = HallwayDirection;
			HallwayDirection = Directions.None;

			if (directionBefore != Directions.None)
			{
				if (onDirectionChanged != null)
				{
					onDirectionChanged.Invoke();
				}
			}
		}

		public void SetDirectionDirect(Directions dir)
		{
			var directionBefore = HallwayDirection;
			this.HallwayDirection = dir;

			if (directionBefore != dir)
			{
				if (onDirectionChanged != null)
				{
					onDirectionChanged.Invoke();
				}
			}
		}
	}

	HallwayDirectionHandler hallwayDirectionHandler;

	private void Awake()
	{
		hallwayDirectionHandler = new HallwayDirectionHandler(OnHallwayDirectionChanged);
		sectorBGRenderer = GetComponent<Renderer>();
	}

	private void Update()
	{
		directionText.text = BuildDirectionText(hallwayDirectionHandler.HallwayDirection);
	}

	public void ConstructHallway(Directions dir)
	{
		hallwayDirectionHandler.ConstructHallway(dir);
	}

	public void DestroyHallway(Directions dir)
	{
		hallwayDirectionHandler.DestroyHallway(dir);
	}

	public void DestroyEveryHallways()
	{
		hallwayDirectionHandler.DestroyEveryHallways();
	}

	void OnHallwayDirectionChanged()
	{
		if (sectorData == null ||
			sectorData.SectorType.ToString().Contains("Hallway_") || sectorData.SectorType == SectorType.Empty)
		{
			data_sector nextSectorData = data_loader.SectorHallwayFromDirections(
				hallwayDirectionHandler.HallwayDirection);
			SetSectorData(nextSectorData);
		}
		else
		{
			Refresh();
		}
	}

	void ModifyRoomSector(Directions hallwayDirection)
	{
		if (sectorData == null) return;
		if (sectorData.SectorType == SectorType.EnteranceRoom)
		{
			dungeon_sectorModifier_base sectorModifier = sectorTilesGO.GetComponent<dungeon_sectorModifier_base>();
			Directions EnteranceDirection = dungeon_manager.ins.dungeon_loader.dungeonInfo.EnteranceDirection;

			if (EnteranceDirection != Directions.Bottom)
			{
				if (hallwayDirection.Contains(Directions.Bottom))
				{
					sectorModifier.SetBottomTileType(TileType.Empty);
				}
				else
				{
					sectorModifier.SetBottomTileType(TileType.Wall);
				}
			}
			if (EnteranceDirection != Directions.Left)
			{
				if (hallwayDirection.Contains(Directions.Left))
				{
					sectorModifier.SetLeftTileType(TileType.Empty);
				}
				else
				{
					sectorModifier.SetLeftTileType(TileType.Wall);
				}
			}
			if (EnteranceDirection != Directions.Right)
			{
				if (hallwayDirection.Contains(Directions.Right))
				{
					sectorModifier.SetRightTileType(TileType.Empty);
				}
				else
				{
					sectorModifier.SetRightTileType(TileType.Wall);
				}
			}
			if (EnteranceDirection != Directions.Top)
			{
				if (hallwayDirection.Contains(Directions.Top))
				{
					sectorModifier.SetTopTileType(TileType.Empty);
				}
				else
				{
					sectorModifier.SetTopTileType(TileType.Wall);
				}
			}
		}
		else if (sectorData.SectorType == SectorType.MonsterRoom ||
			sectorData.SectorType == SectorType.BossRoom)
		{
			dungeon_sectorModifier_base sectorModifier = sectorTilesGO.GetComponent<dungeon_sectorModifier_base>();

			if (hallwayDirection.Contains(Directions.Bottom))
			{
				sectorModifier.SetBottomTileType(TileType.Empty);
			}
			else
			{
				sectorModifier.SetBottomTileType(TileType.Wall);
			}
			if (hallwayDirection.Contains(Directions.Left))
			{
				sectorModifier.SetLeftTileType(TileType.Empty);
			}
			else
			{
				sectorModifier.SetLeftTileType(TileType.Wall);
			}

			if (hallwayDirection.Contains(Directions.Right))
			{
				sectorModifier.SetRightTileType(TileType.Empty);
			}
			else
			{
				sectorModifier.SetRightTileType(TileType.Wall);
			}
			if (hallwayDirection.Contains(Directions.Top))
			{
				sectorModifier.SetTopTileType(TileType.Empty);
			}
			else
			{
				sectorModifier.SetTopTileType(TileType.Wall);
			}
		}
	}

	string BuildDirectionText(Directions buildFrom)
	{
		if (buildFrom == Directions.None)
		{
			return string.Empty;
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			if (buildFrom.Contains(Directions.Left))
			{
				sb.Append('L');
			}
			if (buildFrom.Contains(Directions.Right))
			{
				sb.Append('R');
			}
			if (buildFrom.Contains(Directions.Top))
			{
				sb.Append('T');
			}
			if (buildFrom.Contains(Directions.Bottom))
			{
				sb.Append('B');
			}
			return sb.ToString();
		}
	}

	public void Initialize(int x, int y, Color initialColor)
	{
		sectorX = x;
		sectorY = y;
		initialSectorColor = initialColor;
		ResetSectorBGColor();
	}

	public void SetSectorData(data_sector sectorData)
	{
		if(sectorData != this.sectorData)
		{
			if(sectorTilesGO != null)
			{
				Destroy(sectorTilesGO);
			}

			if (sectorData.SectorPrefab != null)
			{
				sectorTilesGO = Instantiate(sectorData.SectorPrefab);
				Vector2 sectorPos = dungeon_manager.ins.dungeon_loader.GetSectorPos(sectorX, sectorY);
				sectorTilesGO.transform.position = sectorPos;
			}
			this.sectorData = sectorData;
			Refresh();
		}
	}

	public void Refresh()
	{
		var hallwayDir = hallwayDirectionHandler.HallwayDirection;
		ModifyRoomSector(hallwayDir);
		if(hallwayDir == Directions.None && sectorData.SectorType.ToString().StartsWith("Hallway_"))
		{
			SetSectorData(data_loader.Empty_Sector);
		}
	}

	public void tryCloseAdjacentRooms()
	{
		List<dungeon_sectorRuntime> checkers = new List<dungeon_sectorRuntime>();

		var left = dungeon_manager.ins.dungeon_loader.GetSector(sectorPos + IntPoint.Left);
		var right = dungeon_manager.ins.dungeon_loader.GetSector(sectorPos + IntPoint.Right);
		var up = dungeon_manager.ins.dungeon_loader.GetSector(sectorPos + IntPoint.Up);
		var down = dungeon_manager.ins.dungeon_loader.GetSector(sectorPos + IntPoint.Down);
		if (left != null) checkers.Add(left);
		if (right != null) checkers.Add(right);
		if (up != null) checkers.Add(up);
		if (down != null) checkers.Add(down);

		foreach (var sector in checkers)
		{
			if (sector.sectorData != null && sector.sectorData.SectorType.ToString().Contains("Room"))
			{
				var modifier = sector.sectorTilesGO.GetComponent<dungeon_sectorModifier_base>();
				if (modifier != null)
				{
					Directions dir = dungeon_utils.GetDirection(sectorPos, sector.sectorPos);
					sector.DestroyHallway(dir);
				}
			}
		}
	}

	public void SetSectorBGColor(Color new_color)
	{
		MaterialPropertyBlock mpb = new MaterialPropertyBlock();
		mpb.SetColor("_Color", new_color);
		sectorBGRenderer.SetPropertyBlock(mpb);
	}

	public void ResetSectorBGColor()
	{
		SetSectorBGColor(initialSectorColor);
	}

	public void PlayCannotSelectCoroutine()
	{
		if(cannotSelectCoroutine != null)
		{
			StopCoroutine(cannotSelectCoroutine);
		}
		cannotSelectCoroutine = StartCoroutine(CannotSelectCoroutine());
	}

	Directions snapshot;
	public void OnDragSelected()
	{
		SetSectorBGColor(BG_COLOR_SELECTED);
		snapshot = hallwayDirectionHandler.HallwayDirection;
	}

	public void OnDragCancelled()
	{
		ResetSectorBGColor();
		hallwayDirectionHandler.SetDirectionDirect(snapshot);
	}

	public void OnDragFinished()
	{
		ResetSectorBGColor();
	}

	public void OnClickReset()
	{
		ResetSectorBGColor();
	}

	Coroutine cannotSelectCoroutine;
	IEnumerator CannotSelectCoroutine()
	{		
		Color from = new Color(1, 0, 0, 0.5f);
		SetSectorBGColor(from);

		float fade_time = 0.3f;
		float fade_time_count = 0;
		float alpha_from = 0.5f;
		while (fade_time_count <= fade_time)
		{
			float percentage = fade_time_count / fade_time;
			float alpha = (1 - percentage) * alpha_from;
			Color next_color = new Color(1, 0, 0, alpha);
			SetSectorBGColor(next_color);
			fade_time_count += Time.deltaTime;
			yield return null;
		}
		ResetSectorBGColor();
		cannotSelectCoroutine = null;
	}
}
