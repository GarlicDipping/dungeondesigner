﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class dungeon_utils
{
	public static IntPoint SectorFromWorldPosition(Vector2 worldPos)
	{
		int x = dungeon_manager.ins.dungeon_loader.GetSectorIndexX(worldPos.x);
		int y = dungeon_manager.ins.dungeon_loader.GetSectorIndexY(worldPos.y);
		return new IntPoint(x, y);
	}

	public static IntPoint SectorFromInputPosition(Vector2 inputPos)
	{
		Camera main_cam = dungeon_manager.ins.inputHandler.cam;

		Vector3 world_pos = inputPos;
		world_pos.z = -10;
		world_pos = main_cam.ScreenToWorldPoint(world_pos);
		return SectorFromWorldPosition(world_pos);
	}

	/// <summary>
	/// To 입장에서의 섹터 복도가 열려야 할 방향
	/// ex) To가 From의 위에 있다면 Bottom
	/// </summary>
	/// <param name="from"></param>
	/// <param name="to"></param>
	/// <returns></returns>
	public static Directions GetDirection(IntPoint from, IntPoint to)
	{
		if (to.x == from.x)
		{
			if (to.y > from.y)
			{
				return Directions.Bottom;
			}
			else if (to.y < from.y)
			{
				return Directions.Top;
			}
			else
			{
				return Directions.None;
			}
		}
		else
		{
			if (to.x > from.x)
			{
				return Directions.Left;
			}
			else if (to.x < from.x)
			{
				return Directions.Right;
			}
			else
			{
				return Directions.None;
			}
		}
	}
}
