﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class dungeon_manager : PrefabElement
{
	public enum State
	{
		Edit,
		Play
	}
	public State state;

	public Camera mainCam;
	public dungeon_ui dungeon_ui;
	public dungeon_inputHandler inputHandler;
	public dungeon_loader dungeon_loader;
	public static dungeon_manager ins { get; private set; }
	private void Awake()
	{
		if(ins == null)
		{
			ins = this;
		}
	}

	public override void Initialize(object value)
	{
		data_dungeon test_dungeon = data_loader.Dungeon_Datas[0];
		DungeonInfo info = dungeon_utils_dungeonGenerator.GenerateDungeon(test_dungeon);
		dungeon_loader.LoadDungeon(info);
		state = State.Edit;
		dungeon_ui.Initialize();
	}

	// Update is called once per frame
	void Update()
	{
		if (state == State.Edit)
		{
			UpdateOnEditMode();
		}
		else if(state == State.Play)
		{

		}
	}

	void UpdateOnEditMode()
	{
		if (inputHandler.sectorSelectMode == SectorSelectMode.Clicked)
		{
			dungeon_ui.sectorTypeSelector.gameObject.SetActive(true);
			dungeon_ui.dragOptionSelector.gameObject.SetActive(false);
		}
		else if (inputHandler.sectorSelectMode == SectorSelectMode.Dragged)
		{
			dungeon_ui.dragOptionSelector.gameObject.SetActive(true);
			dungeon_ui.sectorTypeSelector.gameObject.SetActive(false);
		}
		else
		{
			dungeon_ui.sectorTypeSelector.gameObject.SetActive(false);
			dungeon_ui.dragOptionSelector.gameObject.SetActive(false);
		}
	}

	public bool CanBeginPlay()
	{
		return true;
	}

	public void BeginPlay()
	{
		state = State.Play;
	}

	public void StopPlay()
	{
		state = State.Edit;
	}

	public void SetRuntimeSelectedSector(data_sector sectordata)
	{
		IntPoint selected_sector_index = inputHandler.CurrentSectorPointClicked;
		dungeon_sectorRuntime sector = dungeon_loader.GetSector(selected_sector_index.x, selected_sector_index.y);
		sector.SetSectorData(sectordata);

		if(sectordata.SectorType == SectorType.Empty)
		{
			sector.DestroyEveryHallways();
			sector.tryCloseAdjacentRooms();
		}
	}
}
