﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeon_loader : MonoBehaviour
{
	public GameObject SectorBGQuadPrefab;
	public dungeon_sectorRuntime[][] sectorRuntimes;
	public DungeonInfo dungeonInfo { get; private set; }
	public int dungeonWidth
	{
		get
		{
			return dungeonInfo.width;
		}
	}
	public int dungeonHeight
	{
		get
		{
			return dungeonInfo.height;
		}
	}

	public void LoadDungeon(DungeonInfo info)
	{
		this.dungeonInfo = info;

		sectorRuntimes = new dungeon_sectorRuntime[info.height][];
		for(int y = 0; y < info.height; y++)
		{
			sectorRuntimes[y] = new dungeon_sectorRuntime[info.width];
		}

		LoadBGGrid();
		LoadEnterence();
		LoadBossRoom();
	}

	void LoadEnterence()
	{
		IntPoint EnterenceSector = dungeonInfo.dungeonData.EnterenceSector;
		Directions EnterenceDirection = dungeonInfo.dungeonData.EnterenceDirection;
		dungeon_sectorRuntime sector = GetSector(EnterenceSector.x, EnterenceSector.y);

		sector.SetSectorData(data_loader.Enterence_Data);
		var enterenceModifier = sector.sectorTilesGO.GetComponent<dungeon_sectorModifier_base>();
		switch (EnterenceDirection)
		{
			case Directions.Bottom:
				enterenceModifier.SetBottomTileType(TileType.Door);
				enterenceModifier.SetLeftTileType(TileType.Wall);
				enterenceModifier.SetRightTileType(TileType.Wall);
				enterenceModifier.SetTopTileType(TileType.Wall);
				break;
			case Directions.Left:
				enterenceModifier.SetBottomTileType(TileType.Wall);
				enterenceModifier.SetLeftTileType(TileType.Door);
				enterenceModifier.SetRightTileType(TileType.Wall);
				enterenceModifier.SetTopTileType(TileType.Wall);
				break;
			case Directions.Right:
				enterenceModifier.SetBottomTileType(TileType.Wall);
				enterenceModifier.SetLeftTileType(TileType.Wall);
				enterenceModifier.SetRightTileType(TileType.Door);
				enterenceModifier.SetTopTileType(TileType.Wall);
				break;
			case Directions.Top:
				enterenceModifier.SetBottomTileType(TileType.Wall);
				enterenceModifier.SetLeftTileType(TileType.Wall);
				enterenceModifier.SetRightTileType(TileType.Wall);
				enterenceModifier.SetTopTileType(TileType.Door);
				break;
		}
	}

	void LoadBossRoom()
	{
		IntPoint BossSector = dungeonInfo.dungeonData.BossSector;
		dungeon_sectorRuntime sector = GetSector(BossSector);

		sector.SetSectorData(data_loader.BossRoom_Data);
	}

	void LoadBGGrid()
	{
		Vector2 LT = LBFromSectorInfos();
		Color grey = new Color(0.91f, 0.91f, 0.91f);
		Color darkergrey = new Color(0.81f, 0.81f, 0.81f);

		for (int y = 0; y < dungeonHeight; y++)
		{
			for (int x = 0; x < dungeonWidth; x++)
			{
				Color color = darkergrey;
				if (y % 2 == 0)
				{
					if (x % 2 == 0)
					{
						color = grey;
					}
				}
				else
				{
					if (x % 2 == 1)
					{
						color = grey;
					}
				}
				CreateSector(LT, x, y, color);
			}
		}
	}

	public dungeon_sectorRuntime GetSector(int x, int y)
	{
		try
		{
			return sectorRuntimes[y][x];
		}
		catch
		{
			return null;
		}
	}

	public dungeon_sectorRuntime GetSector(IntPoint pos)
	{
		return GetSector(pos.x, pos.y);
	}

	public int GetSectorIndexX(float world_x)
	{
		Vector2 LT = LBFromSectorInfos();
		LT -= new Vector2(2.5f, 2.5f);
		int l = (int)((world_x - LT.x) / 5f);
		l = Mathf.Clamp(l, 0, dungeonWidth - 1);
		return l;
	}

	public int GetSectorIndexY(float world_y)
	{
		Vector2 LT = LBFromSectorInfos();
		LT -= new Vector2(2.5f, 2.5f);
		int t = (int)((world_y - LT.y) / 5f);
		t = Mathf.Clamp(t, 0, dungeonHeight - 1);
		return t;
	}

	public Vector2 GetSectorPos(int x, int y)
	{
		Vector2 LT = LBFromSectorInfos();
		return new Vector2(
				LT.x + x * 5f,
				LT.y + y * 5f
			);
	}

	public Vector2 GetSectorPos(IntPoint pos)
	{
		Vector2 LT = LBFromSectorInfos();
		return new Vector2(
				LT.x + pos.x * 5f,
				LT.y + pos.y * 5f
			);
	}

	public Vector2 LBFromSectorInfos()
	{
		int height_meter = dungeonHeight * 5;
		int width_meter = dungeonWidth * 5;
		Vector2 LB = new Vector2(
				-width_meter / 2f + 2.5f,
				-height_meter / 2f + 2.5f
			);
		return LB;
	}

	void CreateSector(Vector2 LT, int x, int y, Color BGColor)
	{
		Vector3 LT_V3 = LT;
		LT_V3.z = SectorBGQuadPrefab.transform.position.z;
		GameObject BG_Panel = Instantiate(SectorBGQuadPrefab);
		BG_Panel.transform.position = LT_V3 +
			new Vector3(
					x * 5f,
					y * 5f,
					0
				);
		dungeon_sectorRuntime sectorRuntime = BG_Panel.GetComponent<dungeon_sectorRuntime>();
		sectorRuntime.Initialize(x, y, BGColor);
		sectorRuntimes[y][x] = sectorRuntime;
	}
}
