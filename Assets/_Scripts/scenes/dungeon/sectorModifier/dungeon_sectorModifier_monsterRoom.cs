﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeon_sectorModifier_monsterRoom : dungeon_sectorModifier_base
{
	public GameObject Left, Right, Top, Bottom;

	public override void SetBottomTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Bottom.SetActive(false);
				break;
			case TileType.Wall:
				Bottom.SetActive(true);
				break;
		}
	}

	public override void SetLeftTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Left.SetActive(false);
				break;
			case TileType.Wall:
				Left.SetActive(true);
				break;
		}
	}

	public override void SetRightTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Right.SetActive(false);
				break;
			case TileType.Wall:
				Right.SetActive(true);
				break;
		}
	}

	public override void SetTopTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Top.SetActive(false);
				break;
			case TileType.Wall:
				Top.SetActive(true);
				break;
		}
	}
}
