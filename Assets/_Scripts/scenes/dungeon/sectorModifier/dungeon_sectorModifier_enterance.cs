﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeon_sectorModifier_enterance : dungeon_sectorModifier_base
{
	public GameObject Left_Wall, Left_Door;
	public GameObject Top_Wall, Top_Door;
	public GameObject Right_Wall, Right_Door;
	public GameObject Bottom_Wall, Bottom_Door;

	public override void SetBottomTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Bottom_Wall.SetActive(false);
				Bottom_Door.SetActive(false);
				break;
			case TileType.Wall:
				Bottom_Wall.SetActive(true);
				Bottom_Door.SetActive(false);
				break;
			case TileType.Door:
				Bottom_Wall.SetActive(false);
				Bottom_Door.SetActive(true);
				Bottom_Door.GetComponent<Tile_Door>().Close();
				break;
		}
	}

	public override void SetLeftTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Left_Wall.SetActive(false);
				Left_Door.SetActive(false);
				break;
			case TileType.Wall:
				Left_Wall.SetActive(true);
				Left_Door.SetActive(false);
				break;
			case TileType.Door:
				Left_Wall.SetActive(false);
				Left_Door.SetActive(true);
				Left_Door.GetComponent<Tile_Door>().Close();
				break;
		}
	}

	public override void SetRightTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Right_Wall.SetActive(false);
				Right_Door.SetActive(false);
				break;
			case TileType.Wall:
				Right_Wall.SetActive(true);
				Right_Door.SetActive(false);
				break;
			case TileType.Door:
				Right_Wall.SetActive(false);
				Right_Door.SetActive(true);
				Right_Door.GetComponent<Tile_Door>().Close();
				break;
		}
	}

	public override void SetTopTileType(TileType type)
	{
		switch (type)
		{
			case TileType.Empty:
				Top_Wall.SetActive(false);
				Top_Door.SetActive(false);
				break;
			case TileType.Wall:
				Top_Wall.SetActive(true);
				Top_Door.SetActive(false);
				break;
			case TileType.Door:
				Top_Wall.SetActive(false);
				Top_Door.SetActive(true);
				Top_Door.GetComponent<Tile_Door>().Close();
				break;
		}
	}
}
