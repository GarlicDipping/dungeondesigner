﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class dungeon_sectorModifier_base : MonoBehaviour
{
	public abstract void SetTopTileType(TileType type);
	public abstract void SetRightTileType(TileType type);
	public abstract void SetBottomTileType(TileType type);
	public abstract void SetLeftTileType(TileType type);
}
