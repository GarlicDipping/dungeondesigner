﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum SectorSelectMode : int
{
	None,
	Clicked,
	Dragged
}

public class dungeon_inputHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
	public Camera cam;
	public SectorSelectMode sectorSelectMode { get; private set; }
	public IntPoint CurrentSectorPointClicked { get; private set; }
	public dungeon_sectorRuntime CurrentSectorClicked
	{
		get
		{
			if(sectorSelectMode == SectorSelectMode.Clicked)
			{
				return dungeon_manager.ins.dungeon_loader.GetSector(CurrentSectorPointClicked);
			}
			else
			{
				return null;
			}
		}
	}
	List<IntPoint> sectors_dragging;
	DungeonInfo dungeonInfo
	{
		get
		{
			return dungeon_manager.ins.dungeon_loader.dungeonInfo;
		}
	}
	BoxCollider input_collider;
	private void Awake()
	{
		sectors_dragging = new List<IntPoint>();
		Rect camRect = GetCameraRect(cam);
		sectorSelectMode = SectorSelectMode.None;
		input_collider = GetComponent<BoxCollider>();
		input_collider.transform.position = new Vector3(camRect.center.x, camRect.center.y, -cam.transform.position.z);
		input_collider.transform.localScale = new Vector3(camRect.size.x, camRect.size.y, 1);
	}

	static Rect GetCameraRect(Camera cam)
	{
		float dist_from_camera = -cam.transform.position.z;
		Vector3 LB = cam.ScreenToWorldPoint(new Vector3(0, 0, dist_from_camera));
		Vector3 RT = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth, cam.pixelHeight, dist_from_camera));
		Vector3 size = RT - LB;

		Rect r = new Rect(LB.x, LB.y, size.x, size.y);
		return r;
	}

	void OnClick(Vector2 clickedPosition)
	{		
		IntPoint nextSectorPointSelected = dungeon_utils.SectorFromInputPosition(clickedPosition);
		dungeon_sectorRuntime selectedSector = dungeon_manager.ins.dungeon_loader.GetSector(nextSectorPointSelected);

		//Clicked Enterance!
		if (nextSectorPointSelected == dungeonInfo.EnteranceSector)
		{			
			ResetClickedSector();

			CurrentSectorPointClicked = nextSectorPointSelected;
			selectedSector.PlayCannotSelectCoroutine();
			sectorSelectMode = SectorSelectMode.None;
			return;
		}
		else
		{
			//이미 클릭했던 섹터가 존재			
			if (sectorSelectMode == SectorSelectMode.Clicked &&
				CurrentSectorPointClicked == nextSectorPointSelected)
			{				
				sectorSelectMode = SectorSelectMode.None;
				ResetClickedSector();
				return;
			}

			sectorSelectMode = SectorSelectMode.Clicked;
			CurrentSectorPointClicked = nextSectorPointSelected;
			selectedSector.OnDragSelected();
			dungeon_manager.ins.dungeon_ui.sectorTypeSelector.OnSectorRefreshed();
		}
	}

	#region Input Handler

	private enum InputMode
	{
		None,
		PointDown,
		Dragging,
		DragEnd,
	}
	private InputMode inputMode = InputMode.None;

	public void OnBeginDrag(PointerEventData eventData)
	{
		inputMode = InputMode.Dragging;
		var begin_sector = dungeon_utils.SectorFromInputPosition(eventData.position);
		dungeon_manager.ins.dungeon_loader.GetSector(begin_sector).OnDragSelected();
		sectors_dragging.Add(begin_sector);
	}

	public void OnDrag(PointerEventData eventData)
	{
		var current_sector = dungeon_utils.SectorFromInputPosition(eventData.position);
		IntPoint last_sector_dragged = sectors_dragging[sectors_dragging.Count - 1];
		if (sectors_dragging.Count > 1)
		{
			//세 섹터 이상 드래그되었을때 드래그해오던 길을 되돌아갔다
			IntPoint lastlast_sector_dragged = sectors_dragging[sectors_dragging.Count - 2];
			if (lastlast_sector_dragged == current_sector)
			{
				sectors_dragging.RemoveAt(sectors_dragging.Count - 1);
				bool reset = sectors_dragging.Contains(last_sector_dragged) == false;
				//같은 섹터를 2번 이상 지나지 않을 경우엔 더이상 이 섹터를 드래그한 정보가 없으니
				//Deselct 표기
				if (reset)
				{
					dungeon_manager.ins.dungeon_loader.GetSector(last_sector_dragged).OnDragCancelled();
				}
				return;
			}
		}
		if (last_sector_dragged != current_sector)
		{
			dungeon_manager.ins.dungeon_loader.GetSector(current_sector).OnDragSelected();
			sectors_dragging.Add(current_sector);
			ConstructHallways();
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		inputMode = InputMode.DragEnd;
		if (sectors_dragging.Count == 1)
		{
			DeselectDraggingSectors();
			OnClick(eventData.position);
		}
		else if(sectors_dragging.Count > 1)
		{
			//Drag!
			sectorSelectMode = SectorSelectMode.Dragged;
			ConstructHallways();
		}
		else
		{
			//Something Wrong!
			inputMode = InputMode.None;
			sectorSelectMode = SectorSelectMode.None;
			ResetClickedSector();
			DeselectDraggingSectors();
		}
	}

	void ConstructHallways()
	{
		for(int i = 0; i < sectors_dragging.Count - 1; i++)
		{
			var from = sectors_dragging[i];
			var to = sectors_dragging[i + 1];
			Directions to_hallway_dir = dungeon_utils.GetDirection(from, to);
			Directions from_hallway_dir = to_hallway_dir.OppositeDirection();

			var sectorRuntimeFrom = dungeon_manager.ins.dungeon_loader.GetSector(from);
			sectorRuntimeFrom.ConstructHallway(from_hallway_dir);
			var sectorRuntimeTo = dungeon_manager.ins.dungeon_loader.GetSector(to);
			sectorRuntimeTo.ConstructHallway(to_hallway_dir);
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		inputMode = InputMode.PointDown;
		ResetClickedSector();
		DeselectDraggingSectors();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if(inputMode == InputMode.PointDown)
		{
			OnClick(eventData.position);
		}
		inputMode = InputMode.None;
	}

	public void DestroyHallways()
	{
		inputMode = InputMode.None;
		sectorSelectMode = SectorSelectMode.None;

		foreach(var selected_sector in sectors_dragging)
		{
			var sectorRuntime = dungeon_manager.ins.dungeon_loader.GetSector(selected_sector);
			sectorRuntime.DestroyEveryHallways();
			if (sectorRuntime.sectorData.SectorType.ToString().Contains("Room") == false)
			{
				sectorRuntime.SetSectorData(data_loader.Empty_Sector);
				sectorRuntime.tryCloseAdjacentRooms();
			}
		}

		ResetClickedSector();
		DeselectDraggingSectors();
	}

	void DeselectDraggingSectors()
	{
		foreach (var selected_sector in sectors_dragging)
		{
			dungeon_manager.ins.dungeon_loader.GetSector(selected_sector).OnDragFinished();
		}
		sectors_dragging.Clear();
	}

	void ResetClickedSector()
	{
		if(CurrentSectorClicked != null)
		{
			CurrentSectorClicked.OnClickReset();
		}
	}

	#endregion
}