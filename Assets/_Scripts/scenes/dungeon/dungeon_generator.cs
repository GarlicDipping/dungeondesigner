﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// 생성 완료된 던전 정보, 저장 필요
/// </summary>
[Serializable]
public class DungeonInfo
{
	public data_dungeon dungeonData;
	public SectorInfo[][] sectorInfos;
	public int width
	{
		get
		{
			return sectorInfos[0].Length;
		}
	}

	public int height
	{
		get
		{
			return sectorInfos.Length;
		}
	}

	public IntPoint EnteranceSector
	{
		get
		{
			return dungeonData.EnterenceSector;
		}
	}

	public Directions EnteranceDirection
	{
		get
		{
			return dungeonData.EnterenceDirection;
		}
	}

	public IntPoint BossSector
	{
		get
		{
			return dungeonData.BossSector;
		}
	}

	public DungeonInfo(data_dungeon dungeonData, SectorInfo[][] sectorInfos)
	{
		this.dungeonData = dungeonData;
		this.sectorInfos = sectorInfos;
	}
}

[Serializable]
public class SectorInfo
{
	public SectorInfo(data_sector sector_data)
	{
		this.sector_data = sector_data;
	}
	public data_sector sector_data;
}

public static class dungeon_utils_dungeonGenerator
{
	/// <summary>
	/// 추후 랜덤 던전 생성 알고리즘을 통해 DungeonInfo를 만들자
	/// </summary>
	/// <param name="dungeonData"></param>
	/// <returns></returns>
	public static DungeonInfo GenerateDungeon(data_dungeon dungeonData)
	{
		data_sector empty_sector = data_loader.Empty_Sector;

		SectorInfo[][] sectorInfos = new SectorInfo[dungeonData.height][];
		for(int y = 0; y < dungeonData.height; y++)
		{
			sectorInfos[y] = new SectorInfo[dungeonData.width];
			for(int x = 0; x < dungeonData.width; x++)
			{
				sectorInfos[y][x] = new SectorInfo(empty_sector);
			}
		}
		return new DungeonInfo(dungeonData, sectorInfos);
	}
}
