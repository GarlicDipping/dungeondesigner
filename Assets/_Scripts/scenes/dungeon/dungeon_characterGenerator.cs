﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class dungeon_characterGenerator
{
	public static int MAX_STAT = 5;

	private const int HP_IDX = 0;
	private const int MP_IDX = 1;
	private const int STR_IDX = 2;
	private const int INT_IDX = 3;
	private const int DEF_IDX = 4;
	private const int RES_IDX = 5;
	public static character_info create_character(int lv, data_character character)
	{
		int statpoints_left = character.totalStatPoint;

		character_info info = new character_info();
		info.lv = lv;
		info.data_Character = character;

		List<int> statIndexes = new List<int>();
		statIndexes.Add(HP_IDX);	//hp
		statIndexes.Add(MP_IDX);	//mp
		statIndexes.Add(STR_IDX);	//str
		statIndexes.Add(INT_IDX);	//int
		statIndexes.Add(DEF_IDX);	//def
		statIndexes.Add(RES_IDX);	//res

		while(statpoints_left > 0)
		{
			int next_stat_index = Random.Range(0, statIndexes.Count);
			int next_stat = statIndexes[next_stat_index];
			switch (next_stat)
			{
				case HP_IDX:
					info.HP++;
					if(info.HP >= MAX_STAT)
					{
						statIndexes.Remove(HP_IDX);
					}
					break;
				case MP_IDX:
					info.MP++;
					if (info.MP >= MAX_STAT)
					{
						statIndexes.Remove(MP_IDX);
					}
					break;
				case STR_IDX:
					info.STR++;
					if (info.STR >= MAX_STAT)
					{
						statIndexes.Remove(STR_IDX);
					}
					break;
				case INT_IDX:
					info.INT++;
					if (info.INT >= MAX_STAT)
					{
						statIndexes.Remove(INT_IDX);
					}
					break;
				case DEF_IDX:
					info.DEF++;
					if (info.DEF >= MAX_STAT)
					{
						statIndexes.Remove(DEF_IDX);
					}
					break;
				case RES_IDX:
					info.RES++;
					if (info.RES >= MAX_STAT)
					{
						statIndexes.Remove(RES_IDX);
					}
					break;
			}
		}
		return info;
	}

	static int rand_additional_statpoint(int min, int max)
	{
		return Random.Range(0, max + 1);
	}
}
