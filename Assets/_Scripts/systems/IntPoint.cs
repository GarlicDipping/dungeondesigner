﻿
using System;
using System.Text;

[Serializable]
public struct IntPoint
{
	public static IntPoint Left = new IntPoint(-1, 0);
	public static IntPoint Right = new IntPoint(1, 0);
	public static IntPoint Up = new IntPoint(0, 1);
	public static IntPoint Down = new IntPoint(0, -1);

	public int x;
	public int y;
	public IntPoint(int x, int y)
	{
		this.x = x;
		this.y = y;
	}


	public static IntPoint operator +(IntPoint a, IntPoint b)
	{
		return new IntPoint(a.x + b.x, a.y + b.y);
	}

	public static IntPoint operator -(IntPoint a, IntPoint b)
	{
		return new IntPoint(a.x - b.x, a.y - b.y);
	}

	public static bool operator ==(IntPoint a, IntPoint b)
	{
		if (a.x == b.x && a.y == b.y)
		{
			return true;
		}
		return false;
	}

	public static bool operator !=(IntPoint a, IntPoint b)
	{
		if (a.x == b.x && a.y == b.y)
		{
			return false;
		}
		return true;
	}

	public override bool Equals(object obj)
	{
		if (obj == null ||
			obj is IntPoint == false)
		{
			return false;
		}
		else
		{
			IntPoint comparer = (IntPoint)obj;
			return comparer == this;
		}
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	public override string ToString()
	{
		StringBuilder builder = new StringBuilder();
		builder.Append('[');
		builder.Append(x.ToString());
		builder.Append(", ");
		builder.Append(y.ToString());
		builder.Append(']');
		return builder.ToString();
	}
}
