﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PrefabElement : MonoBehaviour
{
	public abstract void Initialize(object value);
}