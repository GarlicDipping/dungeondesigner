﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSwapper : MonoBehaviour
{
	public string resourcesPath;
	public Transform enabledObject { get; private set; }

	public bool SwapTo(string name)
	{
		return SwapTo(name, null);
	}
	public bool SwapTo(string name, object value)
	{
		string path = resourcesPath + name;
		var prefab = Resources.Load(resourcesPath + "/" + name, typeof(GameObject)) as GameObject;

		if (prefab == null)
		{
			Debug.LogError("NoPrefab : " + path);
			return false;
		}
		else
		{
			return SwapTo(prefab, value);
		}
	}

	public bool SwapTo(GameObject prefab)
	{
		return SwapTo(prefab, null);
	}
	public bool SwapTo(GameObject prefab, object value)
	{
		//LogSwapToCaller();
		Debug.Log("SwapTo Called, prefab : " + prefab_path_from_scene() + prefab.name);

		if (prefab == null) return false;

		var target = transform.Find(prefab.name);

		if (target == null)
		{
			target = Instantiate(prefab, transform, false).transform;
			target.name = prefab.name;
		}

		enabledObject = null;
		foreach (Transform child in transform)
		{
			if (child == target)
			{
				enabledObject = child;
				child.gameObject.SetActive(true);
			}
			else
			{
				child.gameObject.SetActive(false);
			}
		}

		if (enabledObject != null)
		{
			enabledObject.GetComponent<PrefabElement>().Initialize(value);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void CloseAll()
	{
		foreach (Transform child in transform)
		{
			child.gameObject.SetActive(false);
		}
	}

	#region Utilities

	string prefab_path_from_scene()
	{
		string result = "";
		Transform parent = transform;
		while (parent != null)
		{
			result = result.Insert(0, parent.name + "/");
			parent = parent.parent;
			if (parent == null)
			{
				break;
			}
		}
		return result;
	}

	void LogSwapToCaller()
	{
		System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(true);

		int target_frame_idx = -1;
		for (int i = 1; i < stackTrace.FrameCount; i++)
		{
			if (stackTrace.GetFrame(i).GetMethod().Name.Equals("SwapTo") == false)
			{
				target_frame_idx = i;
				break;
			}
		}
		if (target_frame_idx == -1)
		{
			return;
		}
		// Get calling method name
		System.Diagnostics.StackFrame targetFrame = stackTrace.GetFrame(target_frame_idx);

		System.Reflection.MethodBase method = targetFrame.GetMethod();
		string method_return_type = (method as System.Reflection.MethodInfo).ReturnType.Name;
		Debug.Log("SwapTo() From " + method.DeclaringType + ".cs (Line : " + targetFrame.GetFileLineNumber() + "), " +
			method_return_type + " " + method.Name);
	}
	#endregion
}
