﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabDestroyer : MonoBehaviour
{
	void OnDisable()
	{
		Destroy(gameObject);
	}
}
